-- Find the last element of a list
myLast :: [a] -> a
myLast [] = error "empty list"
myLast [x] = x
myLast (_:xs) = myLast xs

-- Find the last but one element of a list
myButLast :: [a] -> a
myButLast [] = error "empty list"
myButLast xs = head (reverse (init xs))

-- Find the K'th element of a list.
elementAt :: [a] -> Int -> a
elementAt [] _ = error "index too large"
elementAt (x:_) 1 = x
elementAt (_:xs) n = elementAt xs (n - 1)

-- Find the number of elemments of a list.
myLength :: [a] -> Int
myLength xs = go xs 0
	where 
		go [] n = n
		go (_:ys) n = go ys (n + 1)	

-- Reverse a list
myReverse :: [a] -> [a]
myReverse [] = []
myReverse xs = myLast xs : myReverse (init xs)

-- Find out whether a list is a palindrome
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == myReverse xs
